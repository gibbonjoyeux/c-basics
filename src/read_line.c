/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "basics.h"

////////////////////////////////////////////////////////////////////////////////
/// DATA
////////////////////////////////////////////////////////////////////////////////

typedef	struct t_file	t_file;

struct					t_file {
	int					fd;
	char				*keep;
	size_t				size;
	t_file				*next;
};

////////////////////////////////////////////////////////////////////////////////
/// STATIC FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static inline t_file	*get_fd(t_file **list, int fd) {
	t_file				*new, *cur;

	cur = *list;
	while (cur && cur->fd != fd)
		cur = cur->next;
	if (cur != NULL)
		return cur;
	new = malloc(sizeof(t_file));
	if (new == NULL)
		return NULL;
	new->fd = fd;
	new->keep = NULL;
	new->size = 0;
	new->next = *list;
	*list = new;
	return new;
}

static inline void		remove_fd(t_file **list, int fd) {
	t_file				*cur, *prev;

	cur = *list;
	prev = NULL;
	while (cur != NULL) {
		if (cur->fd == fd) {
			if (prev == NULL)
				*list = cur->next;
			else
				prev->next = cur->next;
			free(cur);
			return;
		}
		prev = cur;
		cur = cur->next;
	}
}

static inline i8		check_string(t_file *file, char **line, size_t total_size,
						ssize_t new_size) {
	char				*line_new;
	size_t				i;

	i = total_size - new_size;
	while (i < total_size) {
		if ((*line)[i] == '\n') {
			file->size = total_size - i - 1;
			if (file->size == 0) {
				file->keep = NULL;
			} else {
				file->keep = malloc(file->size);
				if (file->keep == NULL) {
					mem_del((void**)line);
					return -1;
				}
				mem_cpy(file->keep, *line + i + 1, file->size);
				file->keep[file->size] = 0;
			}
			(*line)[i] = 0;
			line_new = realloc(*line, i + 1);
			if (line_new == NULL)
				free(*line);
			*line = line_new;
			return 1;
		}
		i++;
	}
	return 0;
}

static inline i8		read_file_line(t_file *file, char **line,
						size_t total_size) {
	char				buff[READ_BUFFER + 1];
	char				*line_new;
	ssize_t				new_size;
	i8					check;

	while (1) {
		new_size = read(file->fd, buff, READ_BUFFER);
		if (new_size == 0) {
			return ((*line != NULL) ? 1 : 0);
		} else if (new_size < 0 || mem_chr(buff, 0, new_size)) {
			mem_del((void**)line);
			return -1;
		}
		line_new = realloc(*line, total_size + new_size);
		if (line_new == NULL) {
			free(*line);
			*line = NULL;
			return -1;
		}
		mem_cpy(*line + total_size, buff, new_size);
		total_size += (size_t)new_size;
		check = check_string(file, line, total_size, new_size);
		if (check != 0)
			return check;
	}
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int					read_line(int fd, char **line) {
	static t_file	*file_list = NULL;
	t_file			*file;
	size_t			total_size;
	ssize_t			new_size;
	i8				check;

	if (line == NULL || fd < 0)
		return -1;
	file = get_fd(&file_list, fd);
	if (file == NULL)
		return -1;
	*line = file->keep;
	total_size = file->size;
	new_size = total_size;
	if ((check = check_string(file, line, total_size, new_size)))
		return ((check > 0) ? 1 : -1);
	if ((check = read_file_line(file, line, total_size)) == 1)
		return 1;
	remove_fd(&file_list, fd);
	return check;
}


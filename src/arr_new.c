
#include "basics.h"

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

void		**arr_new(size_t w, size_t h, size_t size) {
	void	**array_ptr;
	int8_t	*array_byte;
	size_t	total_size;
	size_t	y;

	if (w == 0 || h == 0 || size == 0)
		return NULL;
	total_size = (h * sizeof(void*)) + (w * h * size);
	if ((total_size - h * sizeof(void*)) != (w * h * size))
		return NULL;
	array_ptr = (void**)malloc(total_size);
	if (array_ptr == NULL)
		return NULL;
	mem_clean(array_ptr, total_size);
	array_byte = (int8_t*)array_ptr + h * sizeof(void*);
	y = 0;
	while (y < h) {
		array_ptr[y] = array_byte + (y * (w * size));
		y++;
	}
	return (array_ptr);
}

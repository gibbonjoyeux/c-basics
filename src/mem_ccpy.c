
#include "basics.h"

void				*mem_ccpy(void *dst, const void *src, int c, size_t n) {
	size_t			cur;
	unsigned char	*ptr;
	unsigned char	*ptr_2;

	cur = 0;
	ptr = (unsigned char *)dst;
	ptr_2 = (unsigned char *)src;
	while (cur < n) {
		if (ptr_2[cur] == (unsigned char)c) {
			ptr[cur] = ptr_2[cur];
			return (dst + cur + 1);
		} else {
			ptr[cur] = ptr_2[cur];
		}
		cur++;
	}
	return NULL;
}


#include "basics.h"

void		*mem_alloc(size_t size) {
	void	*cur;

	cur = malloc(size);
	if (cur == NULL)
		return NULL;
	mem_clean(cur, size);
	return cur;
}

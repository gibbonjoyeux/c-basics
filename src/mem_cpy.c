
#include "basics.h"

void			*mem_cpy(void *dst, const void *src, size_t n) {
	uint8_t		*mem_dst;
	uint8_t		*mem_src;
	size_t		div;
	uint8_t		mod;
	size_t		i;

	mem_dst = (uint8_t *)dst;
	mem_src = (uint8_t *)src;
	div = n / 8;
	mod = n % 8;
	i = 0;
	while (i < div) {
		*((uint64_t *)mem_dst) = *((uint64_t *)mem_src);
		mem_dst += 8;
		mem_src += 8;
		i++;
	}
	i = 0;
	while (i < mod) {
		mem_dst[i] = mem_src[i];
		i++;
	}
	return dst;
}

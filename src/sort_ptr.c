/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "basics.h"

typedef int(*t_fcomp)(void*, void*);

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void	swap(void **array, int i, int j) {
	void	*tmp;

	tmp = array[i];
	array[i] = array[j];
	array[j] = tmp;
}

static int	partition(void **array, int start, int end, t_fcomp f) {
	int			i, j;
	void		*pivot;

	pivot = array[end];
	i = start;
	for (j = start; j < end; j += 1) {
		if (f(array[j], pivot) < 0) {
			swap(array, i, j);
			i += 1;
		}
	}
	swap(array, i, end);
	return i;
}

static void		quicksort(void **array, int start, int end, t_fcomp f) {
	int			pivot;

	if (start >= end)
		return;
	pivot = partition(array, start, end, f);
	quicksort(array, start, pivot - 1, f);
	quicksort(array, pivot + 1, end, f);
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void			sort_ptr(void **array, int length, t_fcomp f) {
	quicksort(array, 0, length - 1, f);
}

/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "basics.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

ssize_t		read_file(int fd, char **file) {
	char	buffer[READ_BUFFER];
	char	*file_new;
	ssize_t	total_length;
	ssize_t	length;

	if (file == NULL)
		return -1;
	*file = NULL;
	total_length = 0;
	while (1) {
		length = read(fd, buffer, READ_BUFFER);
		/// READ ERROR
		if (length < 0) {
			if (*file != NULL)
				free(*file);
			return -1;
		}
		/// READ ADD
		if (length > 0) {
			file_new = realloc(*file, total_length + length);
			if (file_new == NULL) {
				free(*file);
				*file = NULL;
				return -1;
			}
			*file = file_new;
			mem_cpy(*file + total_length, buffer, length);
			total_length += length;
		}
		/// READ END
		if (length < READ_BUFFER)
			return total_length;
	}
}

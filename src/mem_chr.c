
#include "basics.h"

void			*mem_chr(const void *s, int c, size_t n) {
	char		*ptr;
	uint32_t	i;

	if (s == NULL)
		return NULL;
	ptr = (char*)s;
	i = 0;
	while (++i <= n) {
		if (*ptr == (char)c)
			return (ptr);
		ptr++;
	}
	return NULL;
}
